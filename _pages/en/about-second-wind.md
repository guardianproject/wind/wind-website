---
layout: blocks
lang: en
title: About Second Wind
permalink: "/en/about"
date: 2021-02-08T05:00:00.000+00:00
page_sections:
- template: HERO_GET_STARTED_block
  block: HERO_GET_STARTED_block
  teaser-2-title: About Second Wind
- template: HERO_block
  block: HERO_block
  teaser-paragraph: In a sudden moment, the connection we have to each other and information
    can be disturbed or completely taken away. We've seen this in the worst of times
    in hurricanes, among other unfortunate disasters.  <br> <br> When Hurricane Maria
    hit Puerto Rico and the surrounding areas in 2017, <a href="https://www.freepress.net/sites/default/files/2019-05/connecting_the_dots_the_telecom_crisis_in_puerto_rico_free_press.pdf"
    target="_blank">more than a million of people</a> lost internet and cell phone
    signal. It's times like this, where information can save your life, guiding you
    to safety and services. <br> <br>  Internet and cellular networks today are fragile
    and easily overloaded. There has not been enough investment in alternative networks.
    There's so much potential in the radios, sensors and processing available in the
    cheapest of smartphones and routers to connect people when the global internet
    isn't an option. <br> <br>  At <a href="https://guardianproject.info">Guardian
    Project</a>, we've been working to realize this potential and release open source
    tech that starts with the smartphone in your hand. Second Wind is the beginning.  <br>
    <br> Since 2012, we've worked to develop offline sharing capabilities through
    <a href="https://f-droid.org">F-Droid</a>, an open source, independent app store.
    The apps in the <a href="https://gitlab.com/guardianproject/wind-repo">Second
    Wind repo for F-Droid</a> offer a variety of functions—from navigation, information
    distribution, documentation and wellness. Being moved by the situation in Puerto
    Rico, we’ve included Open Street Maps (OSM) to equip people with the utility offline
    maps. The Second Wind app repo includes map files for Puerto Rico and the Virgin
    Islands. Learn more about Managing offline maps with F-Droid and <a href="https://osmand.net/">OsmAnd</a>.  <br>
    <br> <b>Stepping Back </b> <br> <br>Second Wind is the child of ‘Wind’, a concept
    and code name which represents the holistic thinking and possibilities we're exploring
    associated with affordable, off-grid tech capabilities.  <br> <br>  ‘Wind’, as
    a concept, is a network designed for opportunistic communication and sharing of
    local knowledge. It is built on impermanence, movement, and spontaneity. Wind
    is a direct counterpoint to the metaphor of the Web, a system built upon the concept
    of fixed physical nodes, centralized authorities and permanent links. It is rooted
    in the mindsets and needs of people and communities who face challenges communicating.
    Wind is shaped by the movements and density of people in time and space. Explore
    blog posts and other ideas at <a href="https://guardianproject.info/code/wind">Wind
    Project Website</a>.  <br> <br>  <b> A Special Thanks </b> <br> <br>We want to
    extend a special thank you to Mozilla and the National Science Foundation for
    supporting this work under the <a href="https://guardianproject.info/2018/09/26/wind-is-a-mozilla-national-science-foundation-grand-prize-winner/">Wireless
    Innovation for a Networked Society (WINS) Challenge</a>.
  bg-color: "#FFFFFF"
  text-color: ''
  teaser-button-text: Meet the Team
  teaser-button-url: https://guardianproject.info/team/
  teaser-img: "/assets/img/white-bck.png"
  teaser-title: Bringing you a new strength to connect

---
