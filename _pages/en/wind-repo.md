---
layout: blocks
lang: en
title: Offline Apps
permalink: "/en/repo"
date: 2021-02-08T05:00:00.000+00:00
page_sections:
- template: THRESS_OPTIONS_block
  block: THRESS_OPTIONS_block
  go-head-title: GET THE APPS
  go-head-sub-title: Install F-Droid and add the Second Wind Repo to see our curated
    collection
  go-head-list:
  - img-group:
    - img: "/assets/img/f-droid-download-qr.png"
    title: 1) Install F-Droid Store
    text: 'To get the apps curated for Second Wind, first<br/> <a href="https://f-droid.org/F-Droid.apk" class="general-button" style="background-color:white;" target="_blank">Install Store</a>
     <br/><br/>Scan the QR code above, download it from the
      F-Droid website, or get it from a friend if you''re offline.<br/><br/> For more help with this,
      check out our video: <a href="https://www.youtube.com/watch?v=o-kqQQqb-Sw&list=PL4-CVUWabKWdOPRjWwAFn-26pSEml303U&index=5"
      target="_blank">Install F-Droid.</a>'
  - img-group:
    - img: "/assets/img/second-wind-download-qr.png"
    title: 2) Add Second Wind Apps
    text: 'Add the Second Wind repo to your F-Droid app:<br/>
<a href="https://guardianproject-wind.s3.amazonaws.com/fdroid/repo?fingerprint=182CF464D219D340DA443C62155198E399FEC1BC4379309B775DD9FC97ED97E1"
      target="_blank" class="general-button" style="background-color:white;">Add Apps</a><br/><br/>
	You can also scan the QR code above to do so.<br/><br/>
      For more help with this, check out our video: <a href="https://www.youtube.com/watch?v=y3bjUsd3Qoc&list=PL4-CVUWabKWdOPRjWwAFn-26pSEml303U"
      target="_blank">F-Droid QR Repo Sharing.</a><br/><br/>'
  - img-group:
    - img: "/assets/img/second-wind-download-qr-1.png"
    title: 3) Browse Apps On Phone
    text: Once you've added the Second Wind repo, its curated collection of apps can be found in the 'Categories' view of the F-Droid App Store now on your phone. If you don't see them, go to 'Settings' and turn off all other built-in repositories. This change will allo you to only see the Second Wind apps.<br/><br/><br/>

---
