---
layout: blocks
lang: es 
title: home
permalink: "/es/"
date: 2020-11-20T22:00:00.000+00:00
page_sections:
- template: HERO_block
  block: HERO_block
  teaser-title: No tienes internet? Unete a Segundo Viento.
  teaser-paragraph: Segundo Viento es un sistema de distribución fuera de línea para aplicaciones de Android y mucho mas! Consulta nuestra colección cuidadosamente seleccionada de aplicaciones optimizadas que no necesitan internet para funcionar.
  teaser-img: "/assets/img/teaser.png"
  teaser-button-url: repo/
  teaser-button-text: Empieza Aqui
  bg-color: ''
  text-color: ''
- template: SHORT_INTRO_block
  block: SHORT_INTRO_block
  row: normal
  bg-color: ''
  text-color: black
  img: "/assets/img/off-grid.png"
  title: Cuando no hay electricidad.
  paragraph: Encuentra tu camino con mapas fuera de linea. Obtent el Repositorio Segundo Viento aqui <a href="https://f-droid.org/"
    target="_blank">F-Droid</a> para descubrir mas aplicaciones.
  column:
  - img: "/assets/img/osmand.png"
    title: OsmAnd~
    text: Mapas móviles y navegación
  - img: "/assets/img/trail.png"
    title: Trail Sense
    text: Usa los sensores en tu telefono como tu guia de sobrevivencia cuando exploras areas desconocidas
  - img: "/assets/img/stop.png"
    title: Stop-o-Moto
    text: Usa una foto para crear videos y formatos gif
- template: SHORT_INTRO_block
  block: SHORT_INTRO_block
  row: revers
  bg-color: "#3078EF"
  text-color: white
  img: "/assets/img/during-out.png"
  title: Cuando pasa un desastre.
  paragraph: Guias de sobrevivencia, mapas y mensajeria segura hacen de tu seguridad una prioridad.
    Descarga el Repositorio Segundo Viento aqui <a href="https://f-droid.org/" target="_blank">F-Droid</a>
    para obtener mas mapas.
  column:
  - img: "/assets/img/umbrella-logo.png"
    title: Umbrella
    text: Guias de entrenamiento, primeros auxilios e informacion de sobrevivencia.
  - img: "/assets/img/brair.png"
    title: Briar
    text: Mensajeria segura, donde sea, cuando sea.
  - img: "/assets/img/maps.png"
    title: Maps
    text: Mapas fuera de linea.
- template: SHORT_INTRO_block
  block: SHORT_INTRO_block
  row: normal
  img: "/assets/img/during-disaster.png"
  title: Durante un desastre.
  paragraph: Captura la situacion, encuentra consuelo o graba informacion importante.  Obten el Repositorio Segundo Viento aqui <a href="https://f-droid.org/" target="_blank">F-Droid</a>  para descubrir mas aplicaciones.
  column:
  - img: "/assets/img/medic.png"
    title: Medic Log
    text: Registra datos medicos importantes.
  - img: "/assets/img/proff.png"
    title: ProofMode
    text: Transforma tus fotos y videos en evidencia visual segura y firmada digitalmente.
  - img: "/assets/img/moice.png"
    title: Noice
    text: Reproduce sonidos ambientales relajantes para minimizar tu estres e incrementar tu productividad.
  bg-color: ''
  text-color: ''
- template: SHORT_INTRO_block
  block: SHORT_INTRO_block
  row: revers
  bg-color: "#F5D6CA"
  img: "/assets/img/just.png"
  title: Para entretenerte.
  paragraph: Juegos clasicos y clasificacion de constelaciones que te brindaran horas de entretenimiento. Obten el Repositorio Segundo Viento aqui <a href="https://f-droid.org/" target="_blank">F-Droid</a> para descubrir mas aplicaciones.
  column:
  - img: "/assets/img/crosswords.png"
    title: CrossWords
    text: Un juego similar a Scramble
  - img: "/assets/img/planisphere.png"
    title: Planisphere
    text: Identifica y cataloga estrellas, planetas y constelaciones.
  - img: "/assets/img/mindsweeper-and-solitaire.png"
    title: MineSweeper and Solitare
    text: Juegos Clasicos, diversion ilimitada.
  text-color: ''
- template: THRESS_OPTIONS_block
  block: THRESS_OPTIONS_block
  go-head-title: Continua
  go-head-sub-title: Prueba Segundo Viento.
  go-head-list:
  - img-group:
    - img: "/assets/img/ic_flexible.svg"
    title: 'Es Flexible '
    text: Elige el método de distribución que funciona mejor para ti, el único Hardware que necesitas son telefonos Android.
  - img-group:
    - img: "/assets/img/ic_scalable.svg"
    title: Es Escalable.
    text: 'Comparte con varias personas a la vez. Incluso puedes construir tu propia red local.'
  - img-group:
    - img: "/assets/img/ic_free.svg"
    title: Es Gratis.
    text: El Software es gratis y de codigo abiert.No tiene anuncios o trackeo de terceros.
- template: BRANDS_block
  block: BRANDS_block
  by-title: TRAÍDO PARA TI POR
  by-list:
  - img: "/assets/img/logo-gp.png"
    url: https://guardianproject.info/
  - img: "/assets/img/logo-okt.png"
    url: https://okthanks.com/
  - img: "/assets/img/logo-fdroid.png"
    url: http://fdroid.org/
- template: CTA_block
  block: CTA_block
  about-title: Unete a Segundo Viento.
  about-paragraph: Una colección de aplicaciones optimizadas para funcionar sin conexión y de una manera bastante fácil para compartirlos.
  about-btn-text: Empieza aqui
  about-btn-url: repo/
  by-title: TRAÍDO PARA TI POR

---
