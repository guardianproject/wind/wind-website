---
layout: blocks
lang: sp
title: convene
permalink: "/en/convene"
date: 2021-09-01 04:00:00 +0000
page_sections:
- template: HERO_block
  block: HERO_block
  teaser-title: Simply Connect
  teaser-paragraph: Tired of trying to find the secure messaging app everyone is using?
    Connect on Convene — encrypted chat rooms in the browser available for everyone.
    </br> No phone numbers </br> No account setup </br> No app
  teaser-button-text: Open a Private Room
  teaser-img: "/assets/img/gantas-vaiciulenas-qiadyggiu5w-unsplash.jpg"
  bg-color: ''
  text-color: ''
  teaser-button-url: ''
- template: THRESS_OPTIONS_block
  block: THRESS_OPTIONS_block
  go-head-title: SHARE ANYTHING
  go-head-list:
  - img-group: []
    title: Photos
    text: ''
  - img-group: []
    title: Images
    text: ''
  - img-group: []
    title: Files
    text: ''
  - img-group: []
    text: ''
    title: Voice
  go-head-sub-title: ''

---
