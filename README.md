This is the Wind website! 
 
# clone the project
> clone this repo
 
# install project dependencies
> bundle install

# run jekyll with dependencies
> bundle exec jekyll serve

# build the project
> bundle exec jekyll build
 